import './App.css';
import AllPlayers from './components/All Players/AllPlayers';
import Leaderboard from './components/Leaderboard/Leaderboard';
import Streamers from './components/Streames/Streamers';
import Landingpage from './components/LandingPage/Landingpage';
import Navbar from './components/Navbar/Navbar';
import PlayerProfile from './components/PlayersProfile/PlayerProfile';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
// import PlayerProfile from './components/PlayerProfile/PlayerProfile';

function App() {
  return (
    <div className='app'>
      <BrowserRouter>
      <Navbar/>
      <Routes>
        <Route path='/' element={<Landingpage/>}></Route>
        <Route path='/allplayers' element={<AllPlayers/>} ></Route>
        <Route path='/leaders' element={<Leaderboard/>} ></Route>
        <Route path='/streamers' element={<Streamers/>} ></Route>
        <Route path='/playerprofile' element={<PlayerProfile/>} ></Route>

      </Routes>
      </BrowserRouter>

      {/*  */}
      {/* <Landingpage/> */}
      {/* <AllPlayers/> */}
      {/* <PlayerProfile/>
      <Leaderboard/>
      <Streamers/> */}

    </div>
  );
}

export default App;
