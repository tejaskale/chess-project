import React, { Fragment , useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { actions } from "../context/context";
import "./playerprofile.css";

function PlayerProfile() {
// const [playerProfile,setplayerProfile] = useState({})
let selectedPlayer = useSelector(state=>state.selectedPlayerName)
const dispatch = useDispatch()
  useEffect(() => {
    async function getData (){
        let response = await fetch(`https://api.chess.com/pub/player/${selectedPlayer}`)
        if(response.ok){
          let playerProfile = await response.json();
          dispatch(actions.profile(playerProfile))
          console.log(actions.profile(playerProfile));
        }

    }
    getData ()
  }, []);
  let playerProfile = useSelector(state =>state.playerProfile)

  return (
    <Fragment>
      <div className="container">
        <div className="user-card">
          <div className="img-container">
          <img
            src={playerProfile.avatar}
            alt=""
          />
          </div>
          <div className="player-details">
            <h2>Players Name: {playerProfile.name} </h2>      
            <h2>Username : {playerProfile.username}    </h2>
            <h2>Followers : {playerProfile.followers}   </h2>
            <h2>Location : {playerProfile.location}    </h2>
            <h2>Last Online : {playerProfile.last_online} </h2>
            <h2>Is Streamer : {playerProfile.is_streamer ? "Yes" : "No" } </h2>
          </div>
        </div>
      </div>
    </Fragment>
  );
}

export default PlayerProfile;
