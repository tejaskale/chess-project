import React, { useState, useEffect, Fragment } from 'react';
import "./Streamers.css";

function Streamers() {
    const [streamers , setStreamers] = useState([])
    useEffect(() => {
        async function getData (){
            await fetch(`https://api.chess.com/pub/streamers`)
          .then((response) => response.json())
          .then((data) => setStreamers(data.streamers));
        }
        getData ()
      }, []);
      return (
        <Fragment>
            <div className='streamer-container'>
               {streamers.map((player , index)=>{
                return (
                  <div key={index} className='streamer-card'>
                  <img src={player.avatar} className="player-avatar" alt="" />
                  <div>
                  <h2>Player Name : </h2> <h2> {player.username} </h2>
                  </div>
                  <div className='url-container'>
                    <h2>Twitch URL :</h2>
                    <p><a target="blank" href={player.twitch_url}>{player.twitch_url}</a></p> 
                  </div>
                  <div className='url-container'>
                  <h2>Chess URL: </h2>
                  <p><a target="blank" href={player.url}>{player.url}</a></p> 
                  </div>
                </div>
                )
               })}
            </div>
        </Fragment>
      )
}

export default Streamers