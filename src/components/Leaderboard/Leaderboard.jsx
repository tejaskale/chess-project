import React , {Fragment , useEffect, useState} from 'react';
import "./Leaderboard.css"

function Leaderboard() {
  const [leaderBoard,setLeader] = useState([])


  useEffect(() => {
    async function getData (){
        await fetch(`https://api.chess.com/pub/leaderboards`).
      then((response) => response.json())
      .then((data) =>{setLeader(data.daily) ;console.log(data.daily)});
    }
    getData ()
  }, []);
  return (
    <Fragment>
        <div className='leader-container'>
           {leaderBoard.map((player , index)=>{
            return (
              <div key={index} className='leader-card'>
              <h2>Player ID :   {player.player_id} </h2>
              <h2>Player Name : {player.username} </h2>
              <h2>Score :       {player.score} </h2>
              <h2>Player Rank : {player.rank} </h2>
            </div>
            )
           })}
        </div>
    </Fragment>
  )
}

export default Leaderboard