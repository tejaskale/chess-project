import { createSlice , configureStore } from "@reduxjs/toolkit"
const initialState ={
    playersNames:[],
    selectedPlayerName:"",
    isClickedOnPlayer:false,
    leaderBoard:[],
    playerProfile:{},
    streamers:[]
}
let chessPlayers = createSlice({name:"chessPlayersStats",initialState , 
reducers: {
    clickedOnPlayer(state,action) {
        return {...state,isClickedOnPlayer:!state.isClickedOnPlayer}
    },
    allPlayers(state,action) {
        return {...state, playersNames: action.payload}
    },
    playerSelected(state,action) {
        return {...state,selectedPlayerName:action.payload}
    },
    allLeaders(state,action) {
        return {...state,leaderBoard:action.payload}
    },
    allStreamers(state,action) {
        return {...state,streamers:action.payload}
    },
    profile(state,action) {
        return {...state,playerProfile:action.payload}
    }
}
}) 

export const actions = chessPlayers.actions

const store = configureStore({
    reducer: chessPlayers.reducer
})
export default store;

