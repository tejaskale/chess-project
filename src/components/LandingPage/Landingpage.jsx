import React, { Fragment, useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import Navbar from "../Navbar/Navbar";
import "./landingpage.css";
import smily from "../../Images/thanks.svg"

function Landingpage() {

  let navigate = useNavigate()
  function allPlayers() {
    navigate('/allPlayers')
  }
  function leaders() {
    navigate('/leaders')
  }
  function streamers() {
    navigate('/streamers')
  }
  return (
    <Fragment>
      <div className="text-info-container">
        <p>
          The PubAPI is a read-only REST API that responds with JSON-LD data.
          Our goal is to re-package all currently public data from the website
          and make it available via the PubAPI. "Public Data" is information
          available to people who are not logged in, such as player data, game
          data, and club/tournament information. This excludes private
          information that is restricted to the logged in user, such as game
          chat and conditional moves. This is read-only data. You cannot send
          game-moves or other commands to Chess.com from this system. If you
          wish to send commands, you will be interested in the Interactive API
          releasing later this year.
        </p>
      </div>
      <div className="menu-card-container">
        <div onClick={()=>{allPlayers()}} className="card">
            <h1>
                All Players
            </h1>
            <p>
              Here is the list of all the players playing with us.
              Click Here and you will get the list all players and later 
              you can grab the information about them.
            </p>
        </div>
        <div onClick={(e)=>{leaders()}} className="card">
            <h1>
            Leaderboard Position
            </h1>
            <p>
                List of to 50 Players the playing along with us.
            </p>
        </div>
        <div onClick={(e)=>{streamers()}} className="card">
            <h1>
                Online streamers
            </h1>
            <p>
              List of players playing and streaming online on any platform
              will appear here.
            </p>
        </div>
        
      </div>
      <div className="menu-card-container">
          <div className="card">
              <h1>
                Do you want to solve a chess Puzzle
              </h1>
              <a href="https://www.chess.com/puzzles">
              <h3>
                Click here
              </h3>
              </a>
          </div>  
          <div className="thankyou-card">
              <h1>
                Thank You for Visiting our page
              </h1>
              <img src={smily} className="smily-emoji" alt="" />
          </div>  
      </div>
    </Fragment>
  );
}

export default Landingpage;
