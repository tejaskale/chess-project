import React, { Fragment } from 'react';
import { useNavigate } from 'react-router-dom';
import "./Navbar.css"

function Navbar() {
  let navigate = useNavigate()
  function goHome() {
    navigate('/')
  }
  return (
    <Fragment>
        <div className='navbar'>
            <div>
                <h1 onClick={()=>goHome()}>Chess</h1>
            </div>
            <div className='nav-options' >
                <p>Home</p>
                <p>Contact</p>
                <p>Help</p>
            </div>
        </div>
    </Fragment>
  )
}

export default Navbar