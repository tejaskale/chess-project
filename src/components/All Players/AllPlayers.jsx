import React, { Fragment, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { actions } from "../context/context";
import PlayerProfile from "../PlayersProfile/PlayerProfile";
import "./allPlayers.css";


function AllPlayers() {
    // const [playersNames, setPlayername] = useState([]);
    // const [selectedPlayerName,setSelectedPlayer] = useState();
    // const [isClickedOnPlayer,setIsClickedOnPlayer] = useState(false)
      const dispatch = useDispatch()
      let playersNames = useSelector(state=>state.playersNames)
      let selectedPlayer = useSelector(state=>state.selectedPlayerName)
      let navigate = useNavigate()
      
    useEffect(() => {
        async function getData (){
          const response =  await fetch(`https://api.chess.com/pub/titled/GM`)
          if(response.ok){
            const data =await response.json()
            console.log(data);
           dispatch(actions.allPlayers(data.players))
          }
        }
        getData ()
      }, []);

         function setAplayer(e) {
            // setSelectedPlayer(e.target.innerText);
            // setClicked(true)
            dispatch(actions.playerSelected(e.target.innerText))
            navigate("/playerprofile")
          }

          
  return (
    <Fragment>
        <div className='grid'>
            {playersNames.map((name,index)=>{
                return (
                    <div key={index}>
                        <p onClick={(e)=>setAplayer(e)} className="username">
                        {name}
                        </p>
                    </div>
                )
            })}
        </div>
            
       

    </Fragment>
  )
}

export default AllPlayers